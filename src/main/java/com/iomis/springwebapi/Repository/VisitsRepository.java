package com.iomis.springwebapi.Repository;

import com.iomis.springwebapi.entity.Visits;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VisitsRepository extends CrudRepository<Visits , Integer> {
}
