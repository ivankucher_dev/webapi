package com.iomis.springwebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SpringwebapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringwebapiApplication.class, args);
    }

}
