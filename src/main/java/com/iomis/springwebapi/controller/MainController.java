package com.iomis.springwebapi.controller;


import com.iomis.springwebapi.Repository.VisitsRepository;
import com.iomis.springwebapi.entity.Visits;
import com.samskivert.mustache.Mustache;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.HashMap;

@Controller
@RequestMapping("/")
public class MainController {

    public final VisitsRepository visitsRepository;

    public MainController(VisitsRepository visitsRepository) {
        this.visitsRepository = visitsRepository;
    }

    @GetMapping("/")
    public String index( HashMap<String , String> model){

        model.put("name", "Ivan");
        Visits visits = new Visits();
        visits.setDescription(String.format("Visited at %s", LocalDateTime.now()));

        visitsRepository.save(visits);
        return  "index";
    }

}
