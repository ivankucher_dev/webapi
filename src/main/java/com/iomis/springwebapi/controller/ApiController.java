package com.iomis.springwebapi.controller;


import com.iomis.springwebapi.Repository.VisitsRepository;
import com.iomis.springwebapi.entity.Visits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    final VisitsRepository visitsRepository;

    public ApiController(VisitsRepository visitsRepository) {
        this.visitsRepository = visitsRepository;
    }

    @GetMapping("/visits")
    public Iterable<Visits> getVisits(){
        return visitsRepository.findAll();
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }

}
